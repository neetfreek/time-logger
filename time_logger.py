#!/usr/bin/env python3
"""Update time log spreadsheet to track work hours"""

from _datetime import datetime, date, timedelta
import math

import openpyxl

from pathlib import Path

# Global constants
AMOUNT_WORK_DAYS = 5
AMOUNT_HOURS_EXPECTED_DAY = 8
PATH_WORKBOOK = Path("./time-log.xlsx")
TIME_NOW = datetime.now()
# Workbook constants
COL_DAY = "Day"
COL_WEEK = "Week"
COL_WEEK_CALENDAR = "Week (calendar)"
COL_DATE = "Date"
COL_TIME_START = "Start"
COL_TIME_END = "End"
COL_HOURS = "Hours"
COL_BALANCE_DAY = "Balance (day)"
COL_BALANCE_MONTH = "Balance (month)"
COL_BALANCE_YEAR = "Balance (year)"
COL_HEADERS = [COL_DAY, COL_WEEK, COL_WEEK_CALENDAR, COL_DATE, COL_TIME_START,
               COL_TIME_END, COL_HOURS, COL_BALANCE_DAY, COL_BALANCE_MONTH,
               COL_BALANCE_YEAR]

"""Workbook interactions"""


def _must_create_new_worksheet(workbook):
    global TIME_NOW
    # Return whether current year higher than latest worksheet title
    return TIME_NOW.year > int(workbook.sheetnames[-1])


"""New Worksheet"""


def _create_new_year_worksheet(workbook, path_workbook):
    # Handler to create and set up new worksheet for new year
    worksheet_new = _get_new_worksheet(workbook)
    _set_column_headers(workbook, path_workbook, worksheet_new)
    # Set Day, Week
    _set_new_sheet_day_week(workbook, path_workbook)
    _log_start_information(workbook, path_workbook)


def _get_new_worksheet(workbook):
    # Create new worksheet for new year
    global COL_HEADERS

    worksheet_new = workbook.create_sheet(str(TIME_NOW.year))
    worksheet_new.title = str(TIME_NOW.year)
    workbook.active = worksheet_new
    return worksheet_new


def _set_column_headers(workbook, path_workbook, worksheet):
    # Create column headers
    for i in range(len(COL_HEADERS) - 1):
        worksheet.cell(row=1, column=i + 1).value = COL_HEADERS[i]
    workbook.save(path_workbook)


def _set_new_sheet_day_week(workbook, path_workbook):
    # Increment last worksheet's final day, derive week
    previous_worksheet = workbook[str(TIME_NOW.year - 1)]
    workbook.active = previous_worksheet
    cell_previous_day = _get_last_populated_cell(previous_worksheet, 1)
    worksheet = workbook[str(TIME_NOW.year)]
    workbook.active = worksheet

    worksheet.cell(row=2, column=1).value = cell_previous_day.value + 1
    worksheet.cell(row=2, column=2).value = math.ceil(
        cell_previous_day.value / 7)

    workbook.save(path_workbook)


"""Worksheet interactions"""


def _get_last_populated_cell(worksheet, number_col):
    # Get last cell containing value in specified column
    index_row = 1

    if worksheet.cell(row=index_row, column=number_col).value is None:
        return worksheet.cell(row=index_row, column=number_col)

    for row in worksheet.iter_rows(min_col=number_col, max_col=number_col,
                                   max_row=1000):
        for cell in row:
            if cell.value is None:
                return worksheet.cell(row=index_row - 1, column=number_col)
            index_row += 1


def _get_next_empty_cell(worksheet, number_col):
    cell_previous = _get_last_populated_cell(worksheet, number_col)
    return worksheet.cell(row=cell_previous.row + 1,
                          column=cell_previous.column)


def _log_start_information(workbook, path_workbook):
    # Populate cells for start new record
    worksheet = workbook[str(TIME_NOW.year)]
    workbook.active = worksheet
    # Initialise previous row's cells, values, to be checked or incremented
    cell_previous_day = _get_last_populated_cell(worksheet, 1)
    cell_previous_week = _get_last_populated_cell(worksheet, 2)
    cell_previous_cal_week = _get_last_populated_cell(worksheet, 3)
    cell_previous_date = _get_last_populated_cell(worksheet, 4)
    # Initialise current row's cells to be populated with values
    cell_next_day = _get_next_empty_cell(worksheet, 1)
    cell_next_week = _get_next_empty_cell(worksheet, 2)
    cell_next_cal_week = _get_next_empty_cell(worksheet, 3)
    cell_next_date = _get_next_empty_cell(worksheet, 4)
    cell_next_start = _get_next_empty_cell(worksheet, 5)

    # If not new year
    if type(cell_previous_cal_week.value) is not str:
        # Increment days logged if new day
        if cell_previous_date.value.date() == TIME_NOW.date():
            cell_next_day.value = cell_previous_day.value
        else:
            cell_next_day.value = cell_previous_day.value + 1
        # Increment weeks logged if new week
        if cell_next_day.value % 5 == 1 and cell_next_day.value != \
                cell_previous_day.value:
            cell_next_week.value = cell_previous_week.value + 1
        else:
            cell_next_week.value = cell_previous_week.value
    # Populate values for calendar week, date and time
    cell_next_cal_week.value = TIME_NOW.isocalendar()[1]
    cell_next_date.value = TIME_NOW.date()
    cell_next_start.value = TIME_NOW.time()

    workbook.save(path_workbook)


def _must_log_start_time(workbook):
    # Return if must log start time (i.e. if logged start but not stop)
    worksheet = workbook[str(TIME_NOW.year)]
    workbook.active = worksheet
    cell_previous_start = _get_last_populated_cell(worksheet, 5)
    cell_previous_end = _get_last_populated_cell(worksheet, 6)

    return cell_previous_start.row == cell_previous_end.row


def _log_stop_information(workbook, path_workbook):
    # Populate cells for stop new record
    worksheet = workbook[str(TIME_NOW.year)]
    workbook.active = worksheet
    # Enter day's balance or update with previous entries of same day
    log_stop_day(worksheet)
    # Log hours worked in month if end of month
    log_stop_month(worksheet)

    workbook.save(path_workbook)


def log_stop_day(worksheet):
    # Initialise current row's cells to be populated with values
    cell_current_day = _get_last_populated_cell(worksheet, 1)
    cell_previous_day = worksheet.cell(row=cell_current_day.row - 1, column=1)
    cell_previous_start = _get_last_populated_cell(worksheet, 5)
    cell_next_end = _get_next_empty_cell(worksheet, 6)
    cell_next_hours = _get_next_empty_cell(worksheet, 7)
    cell_prev_hours = worksheet.cell(row=cell_next_hours.row - 1, column=7)
    cell_next_bal_day = _get_next_empty_cell(worksheet, 8)
    # Stop time
    cell_next_end.value = TIME_NOW.time()
    # Day balance hours
    cell_next_hours.value = \
        datetime.combine(
            date.today(), cell_next_end.value) \
        - datetime.combine(
            date.today(), cell_previous_start.value)
    # If log for same day as in previous row
    if cell_previous_day.value == cell_current_day.value:
        cell_next_bal_day.value = cell_prev_hours.value + timedelta(
            hours=-AMOUNT_HOURS_EXPECTED_DAY) + cell_next_hours.value
    else:
        cell_next_bal_day.value = timedelta(
            hours=-AMOUNT_HOURS_EXPECTED_DAY) + cell_next_hours.value


def log_stop_month(worksheet):
    month_today = datetime.now().month
    month_tomorrow = (datetime.now() + timedelta(days=1)).month
    if month_tomorrow != month_today:
        # TODO: Record hours worked in month
        pass


"""Top level functionality"""


def _log_start_or_end_time(workbook):
    # If year has changed since last execution
    if _must_create_new_worksheet(workbook):
        _create_new_year_worksheet(workbook, PATH_WORKBOOK)
    # If adding new entry, add start time
    else:
        if _must_log_start_time(workbook):
            _log_start_information(workbook, PATH_WORKBOOK)
        # If completing existing entry, add end time
        else:
            _log_stop_information(workbook, PATH_WORKBOOK)


"""Program execution"""


def main():
    # Main program execution
    workbook = openpyxl.load_workbook(PATH_WORKBOOK)
    _log_start_or_end_time(workbook)
    workbook.close()


main()
